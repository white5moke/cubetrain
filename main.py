import time

from org.white5moke.blockchain.Block import Block
from org.white5moke.blockchain.Blockchain import Blockchain
from org.white5moke.blockchain.Wallet import Wallet


def load_test_blocks(blockchain):
    b = Block(blockchain.last_block().index + 1, 'value is soul', int(time.time() * 1000), blockchain.last_block().hash)
    blockchain.add_block(b)

    b1 = Block(
        blockchain.last_block().index + 1,
        'b00psi3',
        int(time.time() * 1000),
        blockchain.last_block().hash
    )
    blockchain.add_block(b1)

    b2 = Block(
        blockchain.last_block().index + 1,
        'tremble like a FLOWAH!',
        int(time.time() * 1000),
        blockchain.last_block().hash
    )
    blockchain.add_block(b2)


def start_up():
    wallet = Wallet()

    blockchain = Blockchain()
    # load_test_blocks(blockchain)
    print(blockchain)
    # node = Node()


if __name__ == '__main__':
    start_up()

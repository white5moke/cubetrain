import math
import array


class BlockBuffer:
    message: str
    buffer_len: int = 1024
    block = []

    def __init__(self, buffer_len: int = 1024):
        self.buffer_len = buffer_len

    def set_message(self, msg: str) -> None:
        self.message = msg.strip()

        self.proc()

    def proc(self):
        msg_bytes: bytes = self.message.encode("utf-8")

        y = int(len(msg_bytes) / (self.buffer_len + 1))
        for b in range(len(msg_bytes)):
            the_byte = msg_bytes[b]
            print((b%self.buffer_len))

    def get_block(self):
        return self.block

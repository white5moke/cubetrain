import time


def epoch_millis() -> int:
    return int(time.time() * 1000)
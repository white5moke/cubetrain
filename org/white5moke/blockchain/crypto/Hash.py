from Crypto.Hash import SHA256, RIPEMD160


def sha256(d: bytes) -> bytes:
    h = SHA256.new()
    h.update(d)

    return h.digest()


def ripemd160(d: bytes) -> bytes:
    h = RIPEMD160.new()
    h.update(d)

    return h.digest()

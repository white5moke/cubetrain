import socket
import threading
import time
from enum import Enum

from org.white5moke.blockchain.Blockchain import Blockchain


class ClientCommands(Enum):
    mint = 5
    list = 0


class Client:

    def __init__(self):
        self.host = socket.gethostname()
        self.port = 5000
        self.client_socket = socket.socket()

        self.init()

    def init(self):
        try:
            self.client_socket.connect((self.host, self.port))
            print("connected to sever @ " + self.host)

            self.run()
        except ConnectionRefusedError:
            print('connection to server refused')
        finally:
            self.client_socket.close()

    def run(self):
        message = input("> ")

        while message.lower().strip() != "bye":
            self.client_socket.send(message.encode())  # send a message to the server
            # print("sending `%s` to the server" % str(message))

            data = self.client_socket.recv(Node.buffer_length).decode()  # receive a response from the server
            # print("received from the server: " + data.upper().strip()[:Node.buffer_length-1])
            self.process(data)

            message = input("> ")

        self.client_socket.close()

    def process(self, msg: str):
        cmd_prefix = msg[:4].strip().lower()

        if cmd_prefix == ClientCommands.mint.name:
            print("minting")
        elif cmd_prefix == ClientCommands.list.name:
            print("listing")
        else:
            pass


class Server:
    def __init__(self):
        self.host = socket.gethostname()
        self.port = 5000
        self.server_socket = socket.socket()
        self.init()

    def init(self):
        self.server_socket.bind((self.host, self.port))
        """
        TODO allow service to discover a max of concurrent nodes
        for now, hard set to 10
        """
        self.server_socket.listen(10)

        self.run()

    def run(self):
        conn, address = self.server_socket.accept()  # accept a new connection
        # print("connection from " + str(address))

        while True:
            # receive a data stream. only accepting packets of 1024 bytes
            data = conn.recv(Node.buffer_length).decode()

            if not data:
                break

            # print("from the connected client: " + str(data))
            # let's just kill the server gracefully,
            # when client says `bye`
            if str(data).lower().strip() == "bye":
                conn.close()
                self.server_socket.close()
                return

            conn.send(data.encode())  # send a response beck to client

        conn.close()
        self.server_socket.close()


class Node:
    server: Server = None
    client: Client = None
    buffer_length: int = 1024

    blockchain: Blockchain = None

    def __init__(self):
        self.t1 = threading.Thread(target=self.server_thread, name="server")
        self.t2 = threading.Thread(target=self.client_thread, name="client")

        self.t1.start()
        time.sleep(2)
        print("started...")
        self.t2.start()
        print("running.")

    def set_blockchain(self, chain: Blockchain):
        self.blockchain = chain

    def client_thread(self):
        self.client = Client()

    def server_thread(self):
        self.server = Server()

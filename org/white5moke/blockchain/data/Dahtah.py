import os
from pathlib import PurePath, Path

class Dahtah:
    path: PurePath

    def __init__(self, p: PurePath):
        self.path = p

        # create the db file if it doesn't exist
        self.create()

    def create(self):
        if not os.path.exists(self.path.parent):
            os.makedirs(self.path.parent)

        if not os.path.exists(self.path):
            Path(self.path).touch()

from Crypto.PublicKey import RSA
from Crypto.PublicKey.RSA import RsaKey

from org.white5moke.blockchain.crypto.Hash import ripemd160, sha256


class Wallet:
    private_key: RsaKey

    def __init__(self, private_key: str = ""):
        if private_key:
            # we are provided a string generate key pairs
            self.private_key = RSA.importKey(private_key)
        else:
            self.private_key = RSA.generate(2048)

        public_key = self.private_key.public_key().export_key("DER")
        self.public_key = ripemd160(sha256(public_key))



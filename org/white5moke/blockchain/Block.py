import json
import time

from org.white5moke.blockchain.crypto.Hash import ripemd160, sha256


class Block:
    index: int
    data: str
    timestamp: int = int(time.time() * 1000)
    previous_hash: str
    hash: str
    nonce: int = 0

    def __init__(self, index: int, data: str, timestamp: int, previous_hash: str):
        self.index = index
        self.nonce = 0
        self.data = data
        self.timestamp = timestamp
        self.previous_hash = previous_hash
        self.generate_hash()

    def generate_hash(self):
        prefix = "0" * 2
        s = bytes(json.dumps(self.__dict__), 'utf8')
        h = ripemd160(sha256(sha256(s))).hex()

        self.hash = h
        while not self.hash.startswith(prefix):
            self.nonce += 1
            s = bytes(json.dumps(self.__dict__), 'utf8')
            self.hash = ripemd160(sha256(sha256(s))).hex()

        self.hash = ripemd160(sha256(sha256(bytes(self.hash, 'utf8')))).hex()

    def __str__(self):
        d = self.__dict__

        return json.dumps(d)

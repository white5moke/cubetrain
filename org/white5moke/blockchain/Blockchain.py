import json
import time
from pathlib import PurePath, Path
from typing import List

from org.white5moke.blockchain.Block import Block
from org.white5moke.blockchain.crypto.Hash import ripemd160, sha256
from org.white5moke.blockchain.data.Dahtah import Dahtah


class Blockchain:
    chain: List[Block]
    data_handler: Dahtah

    def __init__(self):
        dh = Dahtah(PurePath(Path.home(), ".blockchain", "chain.db"))
        self.data_handler = dh
        # self.genesis()

    def last_block(self):
        return self.chain[-1]

    @staticmethod
    def hashish(m: str) -> str:
        return str(ripemd160(sha256(sha256(bytes(m, "utf-8")))).hex())

    def genesis(self):
        pass

    def __str__(self):
        return json.dumps([b.__dict__ for b in self.chain])
